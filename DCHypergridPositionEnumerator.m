/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "DCHypergridPositionEnumerator.h"


@implementation DCHypergridPositionEnumerator

-(id)initWithHypergrid:(DCHypergrid *)grid {
  _grid = [grid retain];
  _coords = NULL;
  _dimensions = [grid numberOfDimensions];
  _hypergrid  = [grid cHypergrid];
  return self;
}

-(id)nextObject {
  if (!_coords) {
    _coords = (int *)calloc(_dimensions, sizeof(int));
  }
  else {
    // increment int array held by _coords
    if (!hypergrid_coords_increment(_hypergrid, _coords)) {
      return nil;
    }
  }
  return [DCHypergridPosition positionWithSize:_dimensions data:_coords];
}

-(void)dealloc {
  [_grid release];
  if (_coords) free(_coords);
  [super dealloc];
}

@end


@implementation DCHypergridPositionEnumeratorForValue

-(id)initWithHypergrid:(DCHypergrid *)grid targetValue:(int)value {
  self = [super initWithHypergrid:grid];
  targetValue = value;
  return self;
}

-(id)nextObject {
  if (!_coords) {
    _coords = (int *)calloc(_dimensions, sizeof(int));
    if (hypergrid_get_value(_hypergrid, _coords)==targetValue) {
      return [DCHypergridPosition positionWithSize:_dimensions data:_coords];
    }
  }
  while (hypergrid_coords_increment(_hypergrid, _coords)) {
    if (hypergrid_get_value(_hypergrid, _coords)==targetValue) {
      return [DCHypergridPosition positionWithSize:_dimensions data:_coords];
    }
  }
  return nil;
}

@end
