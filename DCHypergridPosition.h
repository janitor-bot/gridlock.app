/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import <Foundation/Foundation.h>


@interface DCHypergridPosition : NSObject <NSCopying> {
@public
  int *_data;
  int _numberOfDimensions;
}

-(id)initWithSize:(int)cap;
-(id)initWithSize:(int)cap data:(int *)values;
-(id)initWithRow:(int)r column:(int)c;

+(DCHypergridPosition *)positionWithSize:(int)size data:(int *)values;
+(DCHypergridPosition *)positionWithRow:(int)r column:(int)c;

-(DCHypergridPosition *)positionByAddingPosition:(DCHypergridPosition *)pos;
-(DCHypergridPosition *)positionBySubtractingPosition:(DCHypergridPosition *)pos;

-(int)compare:(DCHypergridPosition *)pos;

-(int)numberOfDimensions;

-(int)valueAtIndex:(int)index;

-(int)row;
-(int)column;

-(int *)cArray;

-(void)dealloc;

-(id)propertyListValue;
+(DCHypergridPosition *)positionWithPropertyListValue:(id)value;
+(NSArray *)positionArrayFromPropertyListValueArray:(NSArray *)values;

-(void)debug;

@end
