/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "ImageStore.h"
#import "Preferences.h"

static ImageStore *_sharedInstance;

@implementation ImageStore

+(ImageStore *)defaultStore {
  if (!_sharedInstance) {
    _sharedInstance = [[self alloc] init];
  }
  return _sharedInstance;
}

-(id)init {
  self = [super init];
  colorImageCache  = [[NSMutableDictionary alloc] init];
  bitmapImageCache = [[NSMutableDictionary alloc] init];
  return self;
}

-(void)dealloc {
  [colorImageCache release];
  [bitmapImageCache release];
  [super dealloc];
}

-(NSImage *)bitmapImageWithName:(NSString *)name size:(NSSize)size {
  // bitmapImageCache is indexed by name and size
  NSString *sizekey = NSStringFromSize(size);
  NSMutableDictionary *sizeDict = [bitmapImageCache objectForKey:name];
  NSImage *image = nil;
  if (!sizeDict) {
    [bitmapImageCache setObject:(sizeDict=[NSMutableDictionary dictionary]) forKey:name];
  }
  image = [sizeDict objectForKey:sizekey];
  if (!image) {
    NSImage *bitmapImage = [NSImage imageNamed:name];
    image = [[[NSImage alloc] initWithSize:size] autorelease];
    
    [image lockFocus];
    [[bitmapImage bestRepresentationForDevice:nil] drawInRect:NSMakeRect(0,0,size.width,size.height)];
    [image unlockFocus];
    
    // inelegant way of preventing cache from growing without bound
    if ([sizeDict count]>10) {
      [sizeDict removeAllObjects];
    }
    [sizeDict setObject:image forKey:sizekey];
  }
  return image;
}

-(NSImage *)solidPieceImageWithColor:(NSColor *)color size:(NSSize)size {
  NSString *sizekey = NSStringFromSize(size);
  NSMutableDictionary *sizeDict = [colorImageCache objectForKey:color];
  NSImage *image = nil;
  if (!sizeDict) {
    // inelegant way of preventing cache from growing without bound
    if ([colorImageCache count]>10) {
      [colorImageCache removeAllObjects];
    }
    [colorImageCache setObject:(sizeDict=[NSMutableDictionary dictionary]) forKey:color];
  }
  image = [sizeDict objectForKey:sizekey];
  if (!image) {
    image = [[[NSImage alloc] initWithSize:size] autorelease];
    [image lockFocus];
    [color set];
    [[NSBezierPath bezierPathWithOvalInRect:NSMakeRect(1,1,size.width-2,size.height-2)] fill];
    [image unlockFocus];
    // inelegant way of preventing cache from growing without bound
    if ([sizeDict count]>10) {
      [sizeDict removeAllObjects];
    }
    [sizeDict setObject:image forKey:sizekey];
  }
  return image;  
}

-(NSImage *)pieceImageForPlayer:(int)pnum withSize:(NSSize)size {
  if ([[Preferences sharedInstance] useBitmapPieces]) {
    return [self bitmapImageWithName:[[Preferences sharedInstance] imageNameForPlayerNumber:pnum]
                                size:size];
  }
  else {
    return [self solidPieceImageWithColor:[[Preferences sharedInstance] pieceColorForPlayerNumber:pnum]
                                     size:size];
  }
}

@end
