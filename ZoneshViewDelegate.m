/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "ZoneshViewDelegate.h"
#import "Preferences.h"

@implementation ZoneshViewDelegate
// we want to tint the cell with the color of the player that owns the territory. We then return NO so
// that the piece will be drawn as normal
-(BOOL)drawCellWithValue:(int)value atRow:(int)row column:(int)col inRect:(NSRect)rect forGame:(ZoneshGame *)game {
  int owner = [game playerWithHomeZoneContainingPosition:[DCHypergridPosition positionWithRow:row
                                                                                       column:col]];
  if (owner>0) {
    NSColor *playerColor = [[Preferences sharedInstance] highlightColorForPlayerNumber:owner];
    [[playerColor blendedColorWithFraction:0.75 ofColor:[NSColor whiteColor]] set];
    NSRectFill(rect);
  }
  return NO;
}

@end
