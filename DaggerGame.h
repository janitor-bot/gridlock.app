//
//  DaggerGame.h
//  Gridlock
//
//  Created by Brian on Sun Feb 29 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Game.h"

@interface DaggerGame : Game {

}

-(int)ownerOfPosition:(DCHypergridPosition *)pos;
-(BOOL)isCrownAtPosition:(DCHypergridPosition *)pos;

-(int)numberOfDaggersForPlayer:(int)pnum;
-(int)numberOfCrownsForPlayer:(int)pnum;

@end
