//
//  SabotageAI.m
//  Gridlock
//
//  Created by Brian on 3/28/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "SabotageAI.h"
#import "SabotageGame.h"

@implementation SabotageAI

-(int)utilityForCarrierAtPosition:(id)pos forPlayer:(int)pnum inGame:(SabotageGame *)game {
  int baseCarrierWeight = 20;
  static int carrierDistanceWeights[] = {999,20,16,12,8,5,4,3,2,1};
  static int numDistanceWeights = 9;
  
  id goalpos = [game goalPositionForPlayer:pnum];
  int dist = abs([goalpos row]-[pos row])+abs([goalpos column]-[pos column]);
  if (dist>numDistanceWeights) return baseCarrierWeight;
  else return baseCarrierWeight+carrierDistanceWeights[dist];
}

-(int)relativeUtilityForGame:(SabotageGame *)game player:(int)pnum {
  int pieceWeight = 10;
  
  int oppnum = [game nextPlayerNumber];
  
  int utility = pieceWeight*([[game grid] numberOfCellsWithValue:pnum]-[[game grid] numberOfCellsWithValue:oppnum]);
  id myCarrierPos = [[game grid] positionWithValue:-pnum];
  if (myCarrierPos) utility += [self utilityForCarrierAtPosition:myCarrierPos forPlayer:pnum inGame:game];
  else {
    id oppCarrierPos = [[game grid] positionWithValue:-oppnum];
    if (oppCarrierPos) utility -= [self utilityForCarrierAtPosition:oppCarrierPos forPlayer:oppnum inGame:game];
  }
  return utility;
}

@end
