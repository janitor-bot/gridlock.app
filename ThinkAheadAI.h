//
//  ThinkAheadAI.h
//  Gridlock
//
//  Created by Brian on 3/26/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "GenericAI.h"

@interface ThinkAheadAI : GenericAI {
  NSArray *depths;
}

@end
