/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import <Foundation/Foundation.h>
#include "AccessorMacros.h"

#import "GameConfiguration.h"

@interface GameHistory : NSObject {
  NSMutableArray *gameStates;
  int stateIndex;

  id gameConfiguration;
}

-(id)initWithGameConfiguration:(GameConfiguration *)game;

-(Game *)currentGame;
-(Game *)initialGame;

-(void)recordAndExecuteMove:(NSArray *)move;

-(BOOL)undoMove;
-(BOOL)canUndoMove;
-(BOOL)undoMovesUntilPlayerNumberInArray:(NSArray *)pnums;
-(BOOL)canUndoMovesUntilPlayerNumberInArray:(NSArray *)pnums;

-(BOOL)redoMove;
-(BOOL)canRedoMove;
-(BOOL)redoMovesUntilPlayerNumberInArray:(NSArray *)pnums;
-(BOOL)canRedoMovesUntilPlayerNumberInArray:(NSArray *)pnums;

-(id)propertyList;
-(id)initFromPropertyList:(id)plist;

-(int)numberOfMoves;
-(int)currentMoveNumber;
-(NSArray *)moveWithNumber:(int)movenum;
-(Game *)gameWithNumber:(int)movenum;

-(Game *)previousGame;
-(NSArray *)previousMove;

idAccessor_h(gameConfiguration, setGameConfiguration)

@end
