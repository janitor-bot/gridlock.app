//
//  AtomicAI.h
//  Gridlock
//
//  Created by Brian on Sat Feb 14 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GenericAI.h"

@interface AtomicAI : GenericAI {
  NSArray *utilityArray;
}

@end
