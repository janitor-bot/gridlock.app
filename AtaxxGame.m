/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "AtaxxGame.h"

@implementation AtaxxGame

-(void)reset {
  [super reset];

  [self createGridFromConfiguration];
  
  // handle random walls
  if ([[self configurationInfo] objectForKey:@"randomWalls"]) {
    int nwalls = [[[[self configurationInfo] objectForKey:@"randomWalls"] objectForKey:@"count"] intValue];
    NSString *symmetry = [[[self configurationInfo] objectForKey:@"randomWalls"] objectForKey:@"symmetry"];
    int n=0;
    while (n<nwalls) {
      id pos = [self randomPosition];
      if ([self valueAtPosition:pos]==0) {
        [self setValue:-1 atPosition:pos];
        n++;
        if ([@"horizontal" isEqual:symmetry] || [@"both" isEqual:symmetry]) {
          [self setValue:-1 atRow:[pos row] column:[self numberOfColumns]-1-[pos column]];
          n++;
        }
        if ([@"vertical" isEqual:symmetry] || [@"both" isEqual:symmetry]) {
          [self setValue:-1 atRow:[self numberOfRows]-1-[pos row] column:[pos column]];
          n++;
        }
        if ([@"both" isEqual:symmetry]) {
          [self setValue:-1 atRow:[self numberOfRows]-1-[pos row] column:[self numberOfColumns]-1-[pos column]];
          n++;
        }
      }
    }
  }
}

-(BOOL)prepareMoveSequence:(NSArray *)positions {
  int pnum = [self currentPlayerNumber];
  int oppnum = [self nextPlayerNumber];
  
  [self resetFutureGrid];
  
  if ([positions count]==0) return YES;
  else {
//    DCHypergridPosition *pos0 = [positions objectAtIndex:0];
//    DCHypergridPosition *pos1 = [positions objectAtIndex:1];
    DCHypergridPosition *pos0 = [positions count]>1 ? [positions objectAtIndex:0] : nil;
    DCHypergridPosition *pos1 = [positions lastObject];
    // if this is a jump, remove the starting cell
    if (pos0 && (abs([pos1 row]-[pos0 row])>1 ||
                 abs([pos1 column]-[pos0 column])>1)) {
      [[self futureGrid] setValue:0 atPosition:pos0];
    }
    // the destination cell is always filled
    [[self futureGrid] setValue:pnum atPosition:pos1];
    // change all neighbors of the destination
    {
      int r = [pos1 row];
      int c = [pos1 column];
      int dr;
      for(dr=-1; dr<=1; dr++) {
        if (r+dr>=0 && r+dr<[self numberOfRows]) {
          int dc;
          for(dc=-1; dc<=1; dc++) {
            if (c+dc>=0 && c+dc<[self numberOfColumns]) {
              if (oppnum==[self valueAtRow:r+dr column:c+dc]) {
                [[self futureGrid] setValue:pnum atRow:r+dr column:c+dc];
              }
            }
          }
        }
      }
    }
    return YES;
  }
}

-(BOOL)isGameOver {
  // game is over if all cells are filled, or if either player has no cells
  // therefore, game is not over if and only if grid contains at least 1 cell with each of 0,1,2.
  return (![grid hasCellWithValue:0] ||
          ![grid hasCellWithValue:1] ||
          ![grid hasCellWithValue:2]);  
}

-(NSArray *)allValidMoveSequences {
  NSMutableArray *sequences = [NSMutableArray array];
  NSEnumerator *posEnum = [[self grid] enumeratorForPositionsWithValue:[self currentPlayerNumber]];
  id position;
  while (position=[posEnum nextObject]) {
    NSArray *neighbors = [grid neighborsOfPosition:position distance:2];
    NSEnumerator *nenum = [neighbors objectEnumerator];
    id npos;
    while (npos=[nenum nextObject]) {
      if ([self valueAtPosition:npos]==0) {
        [sequences addObject:[NSArray arrayWithObjects:position,npos,nil]];
      }
    }
  }
  
  if ([sequences count]==0) [sequences addObject:[NSArray array]];
  return sequences;
}

-(BOOL)showScores {
  return YES;
}

-(NSString *)descriptionForMove:(NSArray *)move {
  // If it's a normal move (not a jump), the start position doesn't matter, so only show the second
  // This also keeps the display consistent because of AtaxxAI optimizations that sometimes return
  // only the final position.
  if ([move count]==2) {
    id pos0 = [move objectAtIndex:0];
    id pos1 = [move objectAtIndex:1];
    if (abs([pos0 row]-[pos1 row])<=1 && abs([pos0 column]-[pos1 column])<=1) {
      return [super descriptionForMove:[pos1 arrayWithSelf_]];
    }
  }
  return [super descriptionForMove:move];
}

@end
