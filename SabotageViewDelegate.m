//
//  SabotageViewDelegate.m
//  Gridlock
//
//  Created by Brian on 3/27/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "SabotageViewDelegate.h"
#import "SabotageGame.h"
#import "Preferences.h"
#import "ImageStore.h"

@implementation SabotageViewDelegate


-(BOOL)drawCellWithValue:(int)value atRow:(int)row column:(int)col inRect:(NSRect)rect forGame:(SabotageGame *)game {
  // tint the goal cells
  int owner = [game playerWithGoalPosition:[DCHypergridPosition positionWithRow:row column:col]];
  if (owner>0) {
    NSColor *playerColor = [[Preferences sharedInstance] highlightColorForPlayerNumber:owner];
    [[playerColor blendedColorWithFraction:0.75 ofColor:[NSColor whiteColor]] set];
    NSRectFill(rect);
  }
  if (value>=0) {
    // regular piece, draw normally
    return NO;
  }
  else {
    // ball or ball carrier
    if (-value<=[game numberOfPlayers]) {
      // draw player piece
      NSImage *image = [[ImageStore defaultStore] pieceImageForPlayer:-value withSize:rect.size];
      [image compositeToPoint:rect.origin operation:NSCompositeSourceOver];
    }
    // draw ball indicator
    [[NSColor darkGrayColor] set];
    NSRect ballRect = NSMakeRect(rect.origin.x+rect.size.width/2, rect.origin.y+rect.size.height/6,
                                 rect.size.width/3, rect.size.height/3);
    [[NSBezierPath bezierPathWithOvalInRect:ballRect] fill];
    return NO;
  }    
}


@end
