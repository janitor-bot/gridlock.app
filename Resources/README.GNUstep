Gridlock.app
Version 1.10

This file describes how to build and run Gridlock on GNUstep. See the readme.html file for information on how to play. Differences from the Mac OS X version are noted below.

In order to compile and run Gridlock, you must have the GNUstep core libraries installed.  You can download GNUstep from http://www.gnustep.org. Gridlock was built with gcc 3.3 against the CVS snapshot of GNUstep as of November 26 2005. Earlier or later versions may or may not work correctly.

Building
--------
You can build Gridlock either from the command line or using ProjectCenter. To build from the command line, make sure you have executed the GNUstep configuration script in (GNUstep root)/System/Makefiles/GNUstep.sh for sh or bash, or (GNUstep root)/System/Makefiles/GNUstep.sh for csh. Once this is done, run:

make

To build from ProjectCenter, open the PC.pcproj project file, and build from the Build panel.

If the build succeeds, it should create the Gridlock.app GNUstep application. 

Running
-------
To run from a command line, execute:

openapp Gridlock.app

You can also run the application from GWorkspace by double-clicking it.

Known Issues
------------
The GNUstep version of Gridlock is built from the same source code as version 1.10 for Mac OS X. Network play between GNUstep and Mac OS X machines should work, and games saved on one platform should be readable on the other. The GNUstep version has the following limitations:
- The Mac OS X version detects the number of processors and creates one thread per processor to determine the best move for computer players. The GNUstep version cannot automatically detect the number of processors. To use multiple threads for computing moves, you can set the "CPUPlayerThreads" default to a value greater than 0. (For example, "defaults write Gridlock CPUPlayerThreads 2").
- Resizing the game window does not constrain the game board to maintaining equal width and height, as it does in Mac OS X.
- Animations of captured pieces may be slower than they should, due to issues with view redrawing and offscreen images.
- Statistics and chat/network information are shown in separate windows rather than in drawers. Also, the statistics window does not display the record of moves that appears in Mac OS X.
- Rendezvous support is not available.

Feedback
--------
The GNUstep port of Gridlock is not well tested. I have built and run it only on an Athlon machine running Debian 3.0 (kernel 2.4.18). Please let me know if it does not work on your system. Email me at brian@dozingcatsoftware.com with any bugs, comments, or suggestions.

