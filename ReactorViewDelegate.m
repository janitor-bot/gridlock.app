//
//  ReactorViewDelegate.m
//  Gridlock
//
//  Created by Brian on Tue Feb 17 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import "ReactorViewDelegate.h"
#import "ImageStore.h"

@implementation ReactorViewDelegate

-(BOOL)drawCellWithValue:(int)value atRow:(int)row column:(int)col inRect:(NSRect)rect forGame:(id)game {
  if (value<0) {
    NSRect centerRect = NSInsetRect(rect,rect.size.width/3,rect.size.height/3);
    [[NSColor redColor] set];
    [[NSBezierPath bezierPathWithOvalInRect:centerRect] fill];
    return YES;
  }
  return NO;
}

@end
