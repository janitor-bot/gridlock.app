/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "GravityAI.h"

// for now, this is identical to ConnectFourAI

@implementation GravityAI

-(int)relativeUtilityForGame:(Game *)game player:(int)pnum {
  return [self threatWeightForGame:(GravityGame *)game player:pnum] -
  [self threatWeightForGame:(GravityGame *)game player:[game playerNumberMovingAfterPlayer:pnum]];
}

-(int)threatWeightForGame:(GravityGame *)game player:(int)pnum {
  static int nextPlayerWeights[]    = {0,0, 1,5, 100,500, 1000,50000, 9999999,9999999};
  static int currentPlayerWeights[] = {0,0, 1,5, 500,2500, 999999,999999, 9999999,9999999};

  int *weights = (pnum==[game currentPlayerNumber]) ? currentPlayerWeights : nextPlayerWeights;
  NSArray *threats = [game threatsForPlayer:pnum];
  NSEnumerator *te = [threats objectEnumerator];
  NSDictionary *threat;
  int total = 0;
  while (threat=[te nextObject]) {
    int length = [[threat objectForKey:@"length"] intValue];
    // openings is 1 if both ends are open
    int openings = ([[threat objectForKey:@"openBefore"] intValue]>0 &&
                    [[threat objectForKey:@"openAfter"] intValue]>0);
    if (length>=[game winningLength]) length=[game winningLength];
    total += weights[2*length + openings];
  }
  return total;
}

@end
