/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "RendezvousUtils.h"
#include <netinet/in.h>
#include <arpa/inet.h>

static NSString *rendezvousIdentifier = nil;

Class nsNetServiceClass_() {
  return NSClassFromString(@"NSNetService");
}
Class nsNetServiceBrowserClass_() {
  return NSClassFromString(@"NSNetServiceBrowser");
}

BOOL isRendezvousAvailable_() {
  return (nsNetServiceClass_()!=Nil && nsNetServiceBrowserClass_()!=Nil);
}

void getHostAndPortForNetService_(id service, NSString **hostname, int *port) {
  // adopted from Apple's PicBrowserController sample code (http://developer.apple.com/samplecode/Sample_Code/Networking/PictureSharingBrowser/PicBrowserController.m.htm)
  int i;
  for(i=0; i<[[service addresses] count]; i++) {
    struct sockaddr_in *address = (struct sockaddr_in *)[[[service addresses] objectAtIndex:i] bytes];
    if (address->sin_family==AF_INET) {
      // found IPv4 address
      unsigned char *bytes = (void *)&(address->sin_addr);

      *port = ntohs(address->sin_port);
      *hostname = [NSString stringWithFormat:@"%d.%d.%d.%d", bytes[0], bytes[1], bytes[2], bytes[3]];
      return;
    }
  }
  NSLog(@"Failed to find IPv4 address");
  *port = 0;
  *hostname = nil;
}

NSString *rendezvousIdentifier_() {
  if (!rendezvousIdentifier) {
    long r = random();
    rendezvousIdentifier = [[NSString stringWithFormat:@"%x",r] retain];
  }
  return rendezvousIdentifier;
}
