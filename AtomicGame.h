//
//  AtomicGame.h
//  Gridlock
//
//  Created by Brian on Sun Feb 08 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Game.h"

@interface AtomicGame : Game {
}

-(int)ownerOfPosition:(DCHypergridPosition *)pos;
-(int)ownerOfRow:(int)r column:(int)c;

-(int)numberOfPositionsOwnedByPlayer:(int)pnum;

-(int)maxStablePiecesForPosition:(DCHypergridPosition *)pos;
-(int)maxStablePiecesForRow:(int)r column:(int)c;

@end
