/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "CheckersViewDelegate.h"
#import "ImageStore.h"

@implementation CheckersViewDelegate

idAccessor(alternatingCellColor, setAlternatingCellColor)

-(void)dealloc {
  [self setAlternatingCellColor:nil];
  [super dealloc];
}

-(void)setAlternateCellColorComponents:(NSString *)str {
  NSArray *components = [str componentsSeparatedByString:@" "];
  [self setAlternatingCellColor:[NSColor colorWithCalibratedRed:[[components objectAtIndex:0] floatValue]
                                                          green:[[components objectAtIndex:1] floatValue] 
                                                           blue:[[components objectAtIndex:2] floatValue] 
                                                          alpha:1.0]];
}

-(void)drawKingForPlayer:(int)pnum inRect:(NSRect)rect {
  // for kings, draw 2 slightly smaller pieces
  NSImage *image;
  float offw = rect.size.width/8;
  float offh = rect.size.height/8;
  NSRect smallRect = rect;

  smallRect.size.width -= offw;
  smallRect.size.height -= offh;
  image = [[ImageStore defaultStore] pieceImageForPlayer:pnum withSize:smallRect.size];

  [image compositeToPoint:smallRect.origin operation:NSCompositeSourceOver];

  smallRect.origin.x += offw;
  smallRect.origin.y += offh;
  [image compositeToPoint:smallRect.origin operation:NSCompositeSourceOver];
}

// delegate method to draw kings and cells that are always empty
-(BOOL)drawCellWithValue:(int)value atRow:(int)row column:(int)col inRect:(NSRect)rect forGame:(id)game {
  if ((row+col)%2==1 && [self alternatingCellColor]!=nil) {
    [[self alternatingCellColor] set];
    NSRectFill(rect);
    return YES;
  }
  if (value<0 && value>=-2) {
    [self drawKingForPlayer:-value inRect:rect];
    return YES;
  }
  return NO;
}

@end
