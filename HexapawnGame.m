/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "HexapawnGame.h"


@implementation HexapawnGame

-(BOOL)forwardMovesAllowed {
  return [[self configurationInfo] objectForKey:@"moveForward"]!=nil;
}
-(BOOL)diagonalMovesAllowed {
  return [[self configurationInfo] objectForKey:@"moveDiagonally"]!=nil;
}
-(BOOL)forwardCapturesAllowed {
  return [[self configurationInfo] objectForKey:@"captureForward"]!=nil;  
}
-(BOOL)diagonalCapturesAllowed {
  return [[self configurationInfo] objectForKey:@"captureDiagonally"]!=nil;  
}
-(BOOL)sidewaysCapturesAllowed {
  return [[self configurationInfo] objectForKey:@"captureSideways"]!=nil;  
}


-(void)reset {
  [super reset];
  [self setGrid:[DCHypergrid gridWithRows:[[[self configurationInfo] objectForKey:@"rows"] intValue]
                                  columns:[[[self configurationInfo] objectForKey:@"cols"] intValue]]];
  {
    int rowsToFill = [[[self configurationInfo] objectForKey:@"fillRows"] intValue];
    if (rowsToFill<=0) rowsToFill = 1;
    [self fillFirstRows:rowsToFill];
  }
}

-(BOOL)prepareMoveSequence:(NSArray *)positions {
  [self resetFutureGrid];
  [[self futureGrid] setValue:0 atPosition:[positions objectAtIndex:0]];
  [[self futureGrid] setValue:[self currentPlayerNumber] atPosition:[positions objectAtIndex:1]];
  return YES;
}

-(int)playerAtLastRow {
  int rows = [self numberOfRows];
  int cols = [self numberOfColumns];
  int c;
  // check for player 1 on the top row or player 2 on the bottom row
  for(c=0; c<cols; c++) {
    if ([self valueAtRow:0 column:c]==2) return 2;
    if ([self valueAtRow:rows-1 column:c]==1) return 1;
  }
  return 0;
}

-(NSArray *)allValidMoveSequences {
  int maxc = [self numberOfColumns];
  int pnum = [self currentPlayerNumber];
  int oppnum = [self nextPlayerNumber];
  int dr = (pnum==1) ? +1 : -1;
  
  BOOL canMoveForward = [self forwardMovesAllowed];
  BOOL canMoveDiagonally = [self diagonalMovesAllowed];
  BOOL canCaptureForward = [self forwardCapturesAllowed];
  BOOL canCaptureDiagonally = [self diagonalCapturesAllowed];
  BOOL canCaptureSideways = [self sidewaysCapturesAllowed];
  
  NSMutableArray *sequences = [NSMutableArray array];
  NSEnumerator *pe = [[self grid] enumeratorForPositionsWithValue:pnum];
  id pos;
  
  while (pos=[pe nextObject]) {
    int r = [pos row];
    int c = [pos column];
    int value = 0;
    // check forward
    value = [self valueAtRow:r+dr column:c];
    if ((canMoveForward && value==0) || (canCaptureForward && value==oppnum)) {
      [sequences addObject:[NSArray arrayWithObjects:pos,
        [DCHypergridPosition positionWithRow:r+dr column:c],nil]];
    }
    // check forward diagonals
    if (c>0) {
      value = [self valueAtRow:r+dr column:c-1];
      if ((canMoveDiagonally && value==0) || (canCaptureDiagonally && value==oppnum)) {
        [sequences addObject:[NSArray arrayWithObjects:pos,
          [DCHypergridPosition positionWithRow:r+dr column:c-1],nil]];
      }      
    }
    if (c<maxc-1) {
      value = [self valueAtRow:r+dr column:c+1];
      if ((canMoveDiagonally && value==0) || (canCaptureDiagonally && value==oppnum)) {
        [sequences addObject:[NSArray arrayWithObjects:pos,
          [DCHypergridPosition positionWithRow:r+dr column:c+1],nil]];
      }
    }
    // check sideways (captures only)
    if (canCaptureSideways) {
      if (c>0 && [self valueAtRow:r column:c-1]==oppnum) {
        [sequences addObject:[NSArray arrayWithObjects:pos,
          [DCHypergridPosition positionWithRow:r column:c-1],nil]];        
      }
      if (c<maxc-1 && [self valueAtRow:r column:c+1]==oppnum) {
        [sequences addObject:[NSArray arrayWithObjects:pos,
          [DCHypergridPosition positionWithRow:r column:c+1],nil]];        
      }
    }
  }
  return sequences;
}

-(BOOL)currentPlayerHasMove {
  BOOL canMoveForward = [self forwardMovesAllowed];
  BOOL canMoveDiagonally = [self diagonalMovesAllowed];
  BOOL canCaptureForward = [self forwardCapturesAllowed];
  BOOL canCaptureDiagonally = [self diagonalCapturesAllowed];
  BOOL canCaptureSideways = [self sidewaysCapturesAllowed];
  // stalemate is impossible if players can both move and capturing in the same forward direction
  if ((canMoveDiagonally && canCaptureDiagonally) || (canMoveForward && canCaptureForward)) return YES;
  else {
    int maxc = [self numberOfColumns];
    int pnum = [self currentPlayerNumber];
    int oppnum = [self nextPlayerNumber];
    int dr = (pnum==1) ? +1 : -1;
    NSEnumerator *pe = [[self grid] enumeratorForPositionsWithValue:pnum];
    id pos;
    while (pos=[pe nextObject]) {
      int r = [pos row];
      int c = [pos column];
      int value = 0;
      // check forward
      value = [self valueAtRow:r+dr column:c];
      if ((canMoveForward && value==0) || (canCaptureForward && value==oppnum)) return YES;
      // check forward diagonals
      if (c>0) {
        value = [self valueAtRow:r+dr column:c-1];
        if ((canMoveDiagonally && value==0) || (canCaptureDiagonally && value==oppnum)) return YES;
      }
      if (c<maxc-1) {
        value = [self valueAtRow:r+dr column:c+1];
        if ((canMoveDiagonally && value==0) || (canCaptureDiagonally && value==oppnum)) return YES;
      }
      // check sideways (captures only)
      if (canCaptureSideways) {
        if (c>0 && [self valueAtRow:r column:c-1]==oppnum) return YES;
        if (c<maxc-1 && [self valueAtRow:r column:c+1]==oppnum) return YES;
      }
    }
  }
  return NO;
}

-(int)winningPlayer {
  int pnum = [self playerAtLastRow];
  if (pnum!=0) return pnum;
  // if no moves available, next player wins
  return [self nextPlayerNumber];
}

-(BOOL)isGameOver {
  return ([self playerAtLastRow]!=0 || ![self currentPlayerHasMove]);
}

@end
