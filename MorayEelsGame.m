/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "MorayEelsGame.h"
#import "Game.h"

@implementation MorayEelsGame

-(BOOL)prepareMoveSequence:(NSArray *)positions {
  BOOL result = [super prepareMoveSequence:positions];
  // unlike Ataxx, there are no "growth" moves, so the first position is always removed
  [[self futureGrid] setValue:0 atPosition:[positions objectAtIndex:0]];
  return result;
}

-(void)appendMovesForPlayer:(int)pnum fromPosition:(id)pos intoArray:(NSMutableArray *)moves {
  int r = [pos row];
  int c = [pos column];
  if (pnum==1) {
    // Octopussies move forward and diagonally forward (up the board)
    if (r<[self numberOfRows]-1) {
      if ([self valueAtRow:r+1 column:c]==0) {
        [moves addObject:[NSArray arrayWithObjects:pos, [DCHypergridPosition positionWithRow:r+1 column:c], nil]];
      }
      if (c>0 && [self valueAtRow:r+1 column:c-1]==0) {
        [moves addObject:[NSArray arrayWithObjects:pos, [DCHypergridPosition positionWithRow:r+1 column:c-1], nil]];
      }
      if (c<[self numberOfColumns]-1 && [self valueAtRow:r+1 column:c+1]==0) {
        [moves addObject:[NSArray arrayWithObjects:pos, [DCHypergridPosition positionWithRow:r+1 column:c+1], nil]];
      }
    }
  }
  else {
    // Moray eels move forward or diagonally forward like Octopussies (down the board),
    // or up to 2 sideways or 2 sideways and 1 back
    int dr, dc;
    for(dr=-1; dr<=1; dr++) {
      for(dc=-2; dc<=2; dc++) {
        if (dr>=0 || (dc>-2 && dc<2)) {
          if (r+dr>=0 && r+dr<[self numberOfRows] && c+dc>=0 && c+dc<[self numberOfColumns] &&
              [self valueAtRow:r+dr column:c+dc]==0) {
            [moves addObject:[NSArray arrayWithObjects:pos, [DCHypergridPosition positionWithRow:r+dr column:c+dc], nil]];
          }
        }
      }
    }
  }
}

-(NSArray *)allValidMoveSequences {
  int pnum = [self currentPlayerNumber];
  NSMutableArray *moves = [NSMutableArray array];
  NSEnumerator *posEnum = [[self grid] enumeratorForPositionsWithValue:pnum];
  id position;
  while (position=[posEnum nextObject]) {
    [self appendMovesForPlayer:pnum fromPosition:position intoArray:moves];
  }

  if ([moves count]==0) [moves addObject:[NSArray array]];
  return moves;
}

-(BOOL)isGameOver {
  int pnum = [self currentPlayerNumber];
  NSMutableArray *moves = [NSMutableArray array];
  NSEnumerator *posEnum = [[self grid] enumeratorForPositionsWithValue:pnum];
  id position;
  while (position=[posEnum nextObject]) {
    [self appendMovesForPlayer:pnum fromPosition:position intoArray:moves];
    if ([moves count]>0) return NO;
  }
  return YES;
}

-(int)winningPlayer {
  if ([self currentPlayerNumber]==1) {
    // Octopussies win if they can't move but have gotten a piece to the last row
    int r = [self numberOfRows]-1;
    int c;
    for(c=[self numberOfColumns]-1; c>=0; c--) {
      if ([self valueAtRow:r column:c]==1) return 1;
    }
  }
  return [self nextPlayerNumber];
}

-(BOOL)showScores {
  return NO;
}

@end
