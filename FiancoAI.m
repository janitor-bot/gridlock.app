//
//  FiancoAI.m
//  Gridlock
//
//  Created by Brian on Thu Mar 11 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import "FiancoAI.h"


@implementation FiancoAI

-(int)utilityForGame:(Game *)game player:(int)pnum {
  int maxr = [game numberOfRows];
  int baserow = (pnum==1) ? 0 : maxr-1;
  int dr = (pnum==1) ? 1 : -1;
  
  int utility = 0;
  int maxUnblockedDist = 0;
  NSEnumerator *ce = [[game grid] enumeratorForPositionsWithValue:pnum];
  id pos;
  while (pos=[ce nextObject]) {
    int dist = abs([pos row]-baserow);
    utility += 100+dist*dist;
    
    if (dist>maxUnblockedDist && dist>maxr-4) {
      // check for unblocked path to end
      int oppnum = [game playerNumberMovingAfterPlayer:pnum];
      int r0 = [pos row];
      int c0 = [pos column];
      BOOL unblocked = YES;
      int d;
      for(d=1; dist+d<=maxr && unblocked; d++) {
        int r = r0+d*dr;
        int dc;
        int crange = ([game currentPlayerNumber]==pnum) ? d-1 : d;
        for(dc=-crange; dc<=crange && unblocked; dc++) {
          int c = c0+dc;
          if ([game isValidRow:r column:c] && oppnum==[game valueAtRow:r column:c]) unblocked = NO;
        }
      }
      if (unblocked) {
        maxUnblockedDist = dist;
        //NSLog(@"Found unblocked piece at %@ with distance %d", pos, dist);
      }
      
    }
  }
  return utility+10000*maxUnblockedDist;
}

-(int)relativeUtilityForGame:(Game *)game player:(int)pnum {
  return [self utilityForGame:game player:pnum] - [self utilityForGame:game player:[game playerNumberMovingAfterPlayer:pnum]];
}

@end
