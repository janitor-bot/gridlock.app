//
//  TourneyAI.h
//  Gridlock
//
//  Created by Brian on Fri Jan 02 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CheckersAI.h"

@interface TourneyAI : GenericAI {
  int regWeight;
  int kingMinWeight;
  int kingMaxWeight;
}

@end
