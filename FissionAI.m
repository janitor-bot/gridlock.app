//
//  FissionAI.m
//  Gridlock
//
//  Created by Brian on Sat Nov 22 2003.
//  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
//

#import "FissionAI.h"


@implementation FissionAI

-(int)relativeUtilityForGame:(Game *)game player:(int)pnum {
  // if we're doing the suicide option, negate the score
  if ([[game configurationInfo] objectForKey:@"suicide"]) {
    return -[super relativeUtilityForGame:game player:pnum];
  }
  return [super relativeUtilityForGame:game player:pnum];
}
@end
