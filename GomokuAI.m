/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "GomokuAI.h"

@implementation GomokuAI

-(NSArray *)movesToConsiderForGame:(Game *)g {
  GomokuGame *game = g;
  NSArray *myThreats  = [game threatsForPlayer:[game currentPlayerNumber]];
  //NSArray *oppThreats = [game threatsForPlayer:[game nextPlayerNumber]];
  //NSArray *combinedThreats = [myThreats arrayByAddingObjectsFromArray:oppThreats];
  NSEnumerator *te;
  NSDictionary *threat;
  // win or block
  // (as below, only look for our own wins, otherwise we might block opponent when we could win with XX-XX)
  for(te=[myThreats objectEnumerator]; threat=[te nextObject]; ) {
    int tlen = [[threat objectForKey:@"length"] intValue];
    if (tlen==[game winningLength]-1) {
      DCHypergridPosition *pos = [threat objectForKey:@"position"];
      DCHypergridPosition *dir = [threat objectForKey:@"direction"];
      pos = [pos positionBySubtractingPosition:dir];
      if ([game isPositionValid:pos] && [game valueAtPosition:pos]==0) {
        return [NSArray arrayWithObject:[pos arrayWithSelf_]];
      }
      // try other direction
      pos = [DCHypergridPosition positionWithRow:[pos row]+[game winningLength]*[dir row]
                                          column:[pos column]+[game winningLength]*[dir column]];
      if ([game isPositionValid:pos] && [game valueAtPosition:pos]==0) {
        return [NSArray arrayWithObject:[pos arrayWithSelf_]];
      }      
    }
  }
  // look for threat of length n-2 open on both ends; make winning move or block
  // unfortunately this misses critical moves like winning or blocking with XX-XX
  /*
  for(te=[combinedThreats objectEnumerator]; threat=[te nextObject]; ) {
    int tlen = [[threat objectForKey:@"length"] intValue];
    if (tlen==[game winningLength]-2) {
      int fwdlen  = [[threat objectForKey:@"openAfter"] intValue];
      int backlen = [[threat objectForKey:@"openBefore"] intValue];
      if (fwdlen>=1 && backlen>=1 && (fwdlen>1 || backlen>1)) {
        NSMutableArray *array = [NSMutableArray array];
        DCHypergridPosition *pos = [threat objectForKey:@"position"];
        DCHypergridPosition *dir = [threat objectForKey:@"direction"];
        if (fwdlen>1) {
          id fwdpos = [DCHypergridPosition positionWithRow:[pos row]+([game winningLength]-2)*[dir row]
                                                    column:[pos column]+([game winningLength]-2)*[dir column]];
          [array addObject:[NSArray arrayWithObject:fwdpos]];
        }
        if (backlen>1) {
          id backpos = [pos positionBySubtractingPosition:dir];
          [array addObject:[NSArray arrayWithObject:backpos]];
        }
        if ([array count]) return array;
      }
    }
  }
  */
  
  return [super movesToConsiderForGame:g];
}

-(int)relativeUtilityForGame:(Game *)game player:(int)pnum {
  return [self threatWeightForGame:(GomokuGame *)game player:pnum] -
  [self threatWeightForGame:(GomokuGame *)game player:[game playerNumberMovingAfterPlayer:pnum]];
}

-(int)threatWeightForGame:(GomokuGame *)game player:(int)pnum {
  static int nextPlayerWeights[]    = {0,0, 1,5, 10,50, 100,500, 1000,50000, 9999999,9999999};
  static int currentPlayerWeights[] = {0,0, 1,5, 10,50, 500,2500, 999999,999999, 9999999,9999999};

  int *weights = (pnum==[game currentPlayerNumber]) ? currentPlayerWeights : nextPlayerWeights;
  NSArray *threats = [game threatsForPlayer:pnum];
  NSEnumerator *te = [threats objectEnumerator];
  NSDictionary *threat;
  int total = 0;
  while (threat=[te nextObject]) {
    int length = [[threat objectForKey:@"length"] intValue];
    // openings is 1 if both ends are open
    int openings = ([[threat objectForKey:@"openBefore"] intValue]>0 &&
                    [[threat objectForKey:@"openAfter"] intValue]>0);
    if (length>=[game winningLength]) length=[game winningLength];
    total += weights[2*length + openings];
  }
  return total;
}



@end
