/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

@interface NSObject (DCArchivingAdditions)
-(NSData *)archivedData_;
+(id)objectWithArchivedData_:(NSData *)data;
@end

@interface NSUserDefaults (DCArchivingAdditions)
-(id)archivedObjectForKey:(id)key;
-(void)setArchivedObject:(id)obj forKey:(id)key;
@end

@interface NSArray (DCMiscAdditions)
-(id)objectWithValue:(id)searchValue forSelector:(SEL)selector withArgument:(id)arg;
-(NSArray *)arrayByIntersectingArray_:(NSArray *)array;
-(NSArray *)arrayByRemovingObjectsFromArray_:(NSArray *)array;
-(NSArray *)valuesByObjectsPerformingSelector:(SEL)sel;
-(NSArray *)valuesByObjectsPerformingSelector:(SEL)sel withObject_:(id)arg;
-(NSArray *)arrayWithPrefix_:(NSArray *)prefix;
-(NSArray *)arrayWithObjectsInRandomOrder_;
-(NSArray *)sortedArrayUsingKey_:(NSString *)key;
-(NSArray *)sortedArrayByCount_;
@end

@interface NSMutableArray (DCMiscAdditions)
-(void)randomizeObjects_;
-(void)sortArrayUsingKey_:(NSString *)key;
-(void)sortArrayByCount_;
@end

@interface NSPopUpButton (DCAppKitAdditions)
-(void)setItemTitles:(NSArray *)titles representedObjects_:(NSArray *)objects;
-(BOOL)selectItemWithRepresentedObject_:(id)obj;
@end

@interface NSTextView (DCAppKitAdditions)
-(void)appendText:(NSString *)text scrollToEnd_:(BOOL)scroll;
@end

/** Resolution independence support. Only available in Tiger, so return the default value of 1.0 if
-userSpaceScaleFactor isn't available.
  */
@interface NSWindow (DCAppKitAdditions)
-(float)userSpaceScaleFactor_;
@end

@interface NSObject (DCMiscAdditions)
-(NSArray *)arrayWithSelf_;
@end
