/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "QuadWrangleGame.h"


@implementation QuadWrangleGame

-(BOOL)prepareMoveSequence:(NSArray *)positions {
  if ([positions count]==1) {
    // drops don't capture
    [self resetFutureGrid];
    [[self futureGrid] setValue:[self currentPlayerNumber] atPosition:[positions lastObject]];
    return YES;
  }
  else {
    // growth and slide moves flip the same as Ataxx
    return [super prepareMoveSequence:positions];
  }
}

-(NSArray *)allValidMoveSequences {
  NSMutableArray *sequences = [NSMutableArray array];
  // get growths and slides
  NSEnumerator *posEnum = [[self grid] enumeratorForPositionsWithValue:[self currentPlayerNumber]];
  id position;
  while (position=[posEnum nextObject]) {
    int dr, dc;
    // find empty cells in all directions
    for(dr=-1; dr<=1; dr++) {
      for(dc=-1; dc<=1; dc++) {
        if (dr!=0 || dc!=0) {
          BOOL done = NO;
          int dist;
          for(dist=1; !done; dist++) {
            int r = [position row]+dist*dr;
            int c = [position column]+dist*dc;
            if ([self isValidRow:r column:c] && [self valueAtRow:r column:c]==0) {
              [sequences addObject:[NSArray arrayWithObjects:position, [DCHypergridPosition positionWithRow:r column:c], nil]];
            }
            else done = YES;
          }
        }        
      }
    }
  }
  // get simple drops
  posEnum = [[self grid] enumeratorForPositionsWithValue:0];
  while (position=[posEnum nextObject]) {
    [sequences addObject:[position arrayWithSelf_]];
  }

  if ([sequences count]==0) [sequences addObject:[NSArray array]];
  return sequences;
}

@end
