gridlock.app (1.10-4) unstable; urgency=medium

  * Ack NMU, thanks gregor herrmann.
  * debian/control (Maintainer): Adopt package on behalf of the GNUstep
    team (Closes: #691825).
    (Uploaders): Add myself.
    (Build-Depends): Require debhelper (>= 9) and gnustep-make (>=
    2.6.6-2); remove obsolete version requirement for libgnustep-gui-dev.
    (Depends): Add ${misc:Depends} and ${gnustep:Depends}.
    (Description): Remove article to placate lintian.
    (Homepage): Move out of description (Closes: #615355).
    (Vcs-Git, Vcs-Browser): New fields.
    (Standards-Version): Claim compliance with 3.9.5 as of this release.
  * debian/compat: Set to 9.
  * debian/source/format: Switch to 3.0 (quilt) and split local
    modifications to:
  * debian/patches/gcc-4.7.patch:
  * debian/patches/non-linux-archs.patch:  While here, also fix the build
    on GNU/Hurd.
  * debian/patches/parentheses.patch: New, make the build less noisy.
  * debian/patches/gcc-warnings.patch: New (Closes: #749755).
  * debian/patches/series: Create.
  * debian/rules: Rewrite for modern debhelper.
  * debian/dirs: Add usr/share/GNUstep, remove pixmaps and the lintian
    override directories.
  * debian/preinst:
  * debian/install: New file.
  * debian/lintian-override: Delete.
  * debian/Gridlock.desktop: Add Keywords field, correct Categories.
  * debian/watch: Replace with a stub/explanation (Closes: #450024).

 -- Yavor Doganov <yavor@gnu.org>  Mon, 04 Aug 2014 22:24:26 +0300

gridlock.app (1.10-3.2) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "FTBFS with GCC-4.7": apply patch from Yavor Doganov.
    (Closes: #667873)

 -- gregor herrmann <gregoa@debian.org>  Sat, 26 May 2012 18:35:44 +0200

gridlock.app (1.10-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix FTBFS with gnustep-gui/1.22 (Closes: #629202).
  * debian/control (Maintainer): Fix Gürkan's email address.

 -- Yavor Doganov <yavor@gnu.org>  Sat, 08 Oct 2011 14:18:16 +0300

gridlock.app (1.10-3) unstable; urgency=low

  * GNUstep transition.
    + Updated debian/rules.
    + Updated debian/dirs.
  * Applied patch to fix FTBFS on GNU/kFreeBSD, thanks to
    Cyril Brulebois. (Closes: #414074)
  * Added a desktop file.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Thu, 04 Oct 2007 13:42:15 +0200

gridlock.app (1.10-2) unstable; urgency=low

  * Rebuild against latest libgnustep-gui-dev.
  * Bump standards version.
  * Updated manual page.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Sun, 17 Sep 2006 23:04:26 +0200

gridlock.app (1.10-1) unstable; urgency=low

  * New upstream release.
  * Bump standards version.
  * Update of build depends for libgnustep-gui0.10-dev.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Mon,  2 Jan 2006 16:23:45 +0100

gridlock.app (1.9-2) unstable; urgency=low

  * Renamed source package to gridlock.app.
  * Updated manual page and debian/rules.
  * Updated debian/control build-depends for GNUstep 0.9.4.
  * Added documentation from homepage.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Sun, 12 Dec 2004 15:29:26 +0100

gridlock (1.9-1) unstable; urgency=high

  * New upstream release.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Sun, 15 Aug 2004 10:56:01 +0200

gridlock (1.8-6) unstable; urgency=low

  * Updated debian/control. (closes: #259482)
  * Renamed binary package name to gridlock.app.
  * Using gnustep-app-wrapper now.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Thu, 15 Jul 2004 18:46:42 +0200

gridlock (1.8-5) unstable; urgency=low

  * Removed versions from build-depends.
  * Updated paths for GNUstep.sh. (closes: #246970)
  * Improved manual page and debian/copyright.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Tue, 30 Mar 2004 00:13:05 +0200

gridlock (1.8-4) unstable; urgency=low

  * Updated debian/gridlock.sh
  * Changed my name to UTF-8 in debian/{changelog,copyright,control}
  * Updated debian/menu to be like in the application info window (drop .app)
  * Added a pointer to read up the details of the license to debian/copyright

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Fri, 26 Mar 2004 10:24:53 +0100

gridlock (1.8-3) unstable; urgency=low

  * Changed all "Gurkan Sengun" to "Gürkan Sengün"

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Thu, 13 Nov 2003 20:29:39 +0100

gridlock (1.8-2) unstable; urgency=low

  * Clean up debian/ directory:
    - debian/rules: removed comments
    - debian/compat: added
    - updated policy standards version

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Thu, 23 Oct 2003 22:29:13 +0200

gridlock (1.8-1) unstable; urgency=low

  * New upstream version.
  * Added lintian/overrides to calm lintian.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Fri, 17 Oct 2003 20:15:27 +0200

gridlock (1.3-1) unstable; urgency=low

  * Initial Release (closes: #179094).

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Fri, 21 Feb 2003 19:59:30 +0100
