//
//  FiancoGame.m
//  Gridlock
//
//  Created by Brian on Sun Mar 07 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import "FiancoGame.h"


@implementation FiancoGame

-(void)reset {
  [super reset];
  [self createGridFromConfiguration];
}  

-(BOOL)prepareMoveSequence:(NSArray *)positions {
  id pos0 = [positions objectAtIndex:0];
  id pos1 = [positions objectAtIndex:1];
  
  [self resetFutureGrid];

  [[self futureGrid] setValue:0 atPosition:pos0];
  [[self futureGrid] setValue:[self valueAtPosition:pos0] atPosition:pos1];
  // remove jumped piece if applicable
  if (abs([pos0 row]-[pos1 row])>1) {
    [[self futureGrid] setValue:0 atRow:([pos0 row]+[pos1 row])/2 column:([pos0 column]+[pos1 column])/2];
  }
  return YES;
}

-(void)appendJumpsFromPosition:(DCHypergridPosition *)pos forPlayer:(int)pnum intoArray:(NSMutableArray *)moves {
  int r = [pos row];
  int c = [pos column];
  int dr = (pnum==1) ? 1 : -1;
  int dc;
  for(dc=-1; dc<=+1; dc+=2) {
    if ([self isValidRow:r+2*dr column:c+2*dc]) {
      // must jump over enemy and into empty square
      int jval = [self valueAtRow:r+dr column:c+dc];
      if (jval>0 && jval!=pnum && [self valueAtRow:r+2*dr column:c+2*dc]==0) {
        [moves addObject:[NSArray arrayWithObjects:pos, [DCHypergridPosition positionWithRow:r+2*dr column:c+2*dc], nil]];
      }
    }
  }
}

-(void)appendNonjumpsFromPosition:(DCHypergridPosition *)pos forPlayer:(int)pnum intoArray:(NSMutableArray *)moves {
  int r = [pos row];
  int c = [pos column];
  int dr = (pnum==1) ? 1 : -1;
  // check left, right, forward
  if ([self isValidRow:r column:c-1] && [self valueAtRow:r column:c-1]==0) {
    [moves addObject:[NSArray arrayWithObjects:pos, [DCHypergridPosition positionWithRow:r column:c-1], nil]];
  }
  if ([self isValidRow:r column:c+1] && [self valueAtRow:r column:c+1]==0) {
    [moves addObject:[NSArray arrayWithObjects:pos, [DCHypergridPosition positionWithRow:r column:c+1], nil]];
  }
  if ([self isValidRow:r+dr column:c] && [self valueAtRow:r+dr column:c]==0) {
    [moves addObject:[NSArray arrayWithObjects:pos, [DCHypergridPosition positionWithRow:r+dr column:c], nil]];
  }
}

-(NSArray *)allValidMoveSequences {
  NSMutableArray *jumps = [NSMutableArray array];
  NSMutableArray *nonjumps = [NSMutableArray array];
  int pnum = [self currentPlayerNumber];
  NSEnumerator *pe = [[self grid] enumeratorForPositionsWithValue:pnum];
  id pos;
  while (pos=[pe nextObject]) {
    [self appendJumpsFromPosition:pos forPlayer:pnum intoArray:jumps];
    if ([jumps count]==0) {
      [self appendNonjumpsFromPosition:pos forPlayer:pnum intoArray:nonjumps];
    }
  }
  return ([jumps count]>0) ? jumps : nonjumps;
}

-(int)playerAtLastRow {
  int c;
  int maxr = [self numberOfRows]-1;
  for(c=[self numberOfColumns]-1; c>=0; c--) {
    if ([self valueAtRow:0 column:c]==2) return 2;
    if ([self valueAtRow:maxr column:c]==1) return 1;
  }
  return 0;
}

-(BOOL)isGameOver {
  return [self playerAtLastRow]>0;
}

-(int)winningPlayer {
  return [self playerAtLastRow];
}

@end
