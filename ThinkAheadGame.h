//
//  ThinkAheadGame.h
//  Gridlock
//
//  Created by Brian on 3/26/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "Game.h"

@interface ThinkAheadGame : Game {
  int p1Score;
  int p2Score;
}

@end
